
$(document).ready(function () {

    $('.menu-tab li a').click(function (event) {

        event.preventDefault();

        $('.menu-tab li').removeClass('selected');
        $(this).parent('li').addClass('selected');

        let imgWidth = '278px';
        let imgHeight = '200px'
        let thisItem = $(this).attr('rel');

        if (thisItem !== "all") {

            $('.item li[rel=' + thisItem + ']').stop()
                .animate({
                    'width': imgWidth,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0

                });

            $('.item li[rel!=' + thisItem + ']').stop()
                .animate({
                    'width': 0,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0

                });
        } else {
            $('.item li').stop()
                .animate({
                    'opacity': 1,
                    'height': imgHeight,
                    'marginRight': 0,
                    'marginLeft': 0,
                    'width': imgWidth,
                });
        }
    })

    $('.item li img').animate({ 'opacity': 1 }).hover(function () {

        $(this).animate({ 'opacity': 1 });

    }, function () {

        $(this).animate({ 'opacity': 1 });

    });

    $('.classForSlick').slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });

    $('.classForSlick2').slick({
        dots: false,
        centerMode: true,
        arrows: true,
        prevArrow: '.myNawLeft',
        nextArrow: '.myNawRight',
        infinite: true,
        speed: 300,
        slidesToScroll: 1,
        adaptiveHeight: true,
        asNavFor: '.classForSlick',
        focusOnSelect: true,
        variableHeight: true,
        variableWidth: true
    });

});

function createNewImg() {

    $('#loadMoreBtn').remove();

    $('.preloader').css('display', 'block');

    setTimeout(getMoreImgs, 2000);

    function getMoreImgs() {

        $('.display-none').removeClass('display-none');
        $('.category-items-second').attr('class', 'category-items');
        $('.category-items').css('display', 'block');
        $('.category-items-second').removeClass('.category-items-second');

        $('.content-amazing').css('min-height', '850px');
        $('.content-amazing').css('height', 'auto');
        $('.preloader').css('display', 'none');
    }

    return false;
}

$('#loadMoreBtn').on('click', createNewImg);

function createNewImgGallaery() {

    $('#loadMoreBtnGallery').remove();
    $('.preloader').css('display', 'block');

    setTimeout(getMoreImgsGallary, 2000);

    function getMoreImgsGallary() {

        $('.display-none').removeClass('display-none');
        $('.masonry-left-two').attr('class', 'masonry-left-two-load');
        $('.masonry-left').css('display', 'block');
        $('.masonry-left-two').removeClass('.masonry-left-two');

        $('.masonry-center-two').attr('class', 'masonry-center-load');
        $('.masonry-center').css('display', 'block');
        $('.masonry-center-two').removeClass('.masonry-center-two-two');

        $('.masonry-right-two').attr('class', 'masonry-right-two-load');
        $('.masonry-right').css('display', 'block');
        $('.masonry-right-two').removeClass('.masonry-right-two');

        $('.preloader').css('display', 'none');
    }

    return false;
}

$('#loadMoreBtnGallery').on('click', createNewImgGallaery);


