(function($) {
    $(function() {
        $("ul.tabs").on("click", "li:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.wrapper-tabs")
                .find("div.tabs-content")
                .removeClass("content-active")
                .eq($(this).index())
                .addClass("content-active");
        });
    });
})($);